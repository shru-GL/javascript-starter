## Instruction

Create a project in Git lab

## Install Npm using below cmd
npm init
npm install -g parcel-bundler

## Following steps
* `mkdir` javascript-starter
* Create **index.html** and call **index.js**
* Create **index.js** with **console.log** statement to see it in console src
* Add start cmd in package.json to easy run in cmd `npm run start`
* Follow the parcel bundler **https://parceljs.org/getting_started.html**
 cmd run `npm install -g parcel-bundler` This will gives the localhost url

## Git commands
* `git init` initialize empty git repository
* `git remote add origin <https://repo-path>`
* `git status`
* To ignore unrelevent file create .gitignore file in VS code and incluse the folders/ and files
* check the git status
* `git add .`
* `git commit -m "Intial commit"`
* `git push -u origin master`
* If any error in push then use `git config --global http.sslVerify false`

